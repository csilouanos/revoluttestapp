//
//  DateExtensions.swift
//  DateExtensions
//
//  Created by Silouanos on 17/09/2018.
//  Copyright © 2018 Silouanos. All rights reserved.
//


import Foundation

extension Date {
    func toStringDefaultFormat() -> String {
        return self.toString(format: .custom("yyyy-MM-dd"))
    }
}

extension String {
    func toDate() -> Date? {
        return Date(fromString: self, format: .custom("yyyy-MM-dd"))
    }
}
