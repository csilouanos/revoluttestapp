//
//  UIImageExtensions.swift
//  RevolutTestApp
//
//  Created by Silouanos on 17/09/2018.
//  Copyright © 2018 Silouanos. All rights reserved.
//
//  Extension (wrapper) that executes operations have to do with downloading image and sets it on image view.

import UIKit
import SDWebImage

extension UIImageView
{
    func setImage(usingURL url: URL, placeholder: UIImage? = nil, success succeed: ((UIImage) -> ())? = nil, failure fail: ((Error) -> ())? = nil)
    {
        self.sd_setImage(with: url, placeholderImage: placeholder, options: UIImageView.sdWebImageOptions) { [weak self] (image, error, cacheType, url) in
            guard let weakSelf = self else {return}
            if let error = error {
                fail?(error)
            }
            else if let image = image {
                weakSelf.image = image
                succeed?(image)
            }
        }
    }
    
    static var sdWebImageOptions: SDWebImageOptions {
        var options = SDWebImageOptions()
        options.insert(.continueInBackground)
        options.insert(.retryFailed)
        options.insert(.highPriority)
        return options
    }
}
