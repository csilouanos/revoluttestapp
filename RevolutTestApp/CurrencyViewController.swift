//
//  ViewController.swift
//  RevolutTestApp
//
//  Created by Silouanos on 17/09/2018.
//  Copyright © 2018 Silouanos. All rights reserved.
//

import UIKit

class CurrencyViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: CurrencyTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: CurrencyTableViewCell.identifier)
            tableView.keyboardDismissMode = .onDrag
            //Helps us prevent cells' glitching during updates.
            tableView.estimatedRowHeight = 0
        }
    }
    
    private var rateCurrencies = [CurrencyRate]()
    private var baseCurrency = BaseCurrency(currency: "EUR", amount: 1.0)
    
    private var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getInitialRates()
        startUpdateMechanism()
    }
    
    private func startUpdateMechanism() {
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
            Currency.getCurrencyRates(basedOn: self.baseCurrency) { (success, latestCurrencies, baseCurrency) in
                if success {
                    DispatchQueue.main.async {
                        self.rateCurrencies = latestCurrencies
                        self.reloadDataOfRateCurrencies()
                    }
                }
            }
        })
    }
    
    private func getInitialRates() {
        Currency.getCurrencyRates() { (success, latestCurrencies, baseCurrency) in
            if success {
                DispatchQueue.main.async {
                    guard let base = baseCurrency else {
                        return
                    }
                    //Take the base currency from server.
                    self.baseCurrency = base
                    self.rateCurrencies = latestCurrencies
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    /**
        Reloads data of all rows except the first one that holds the base currency.
    */
    private func reloadDataOfRateCurrencies() {
        if rateCurrencies.count == 0 {
            return
        }
        var rateCurrenciesIndexPaths = [IndexPath]()
        (1...rateCurrencies.count).forEach({
            rateCurrenciesIndexPaths.append(IndexPath(item: $0, section: 0))
        })
        UIView.performWithoutAnimation {
            self.tableView.reloadRows(at: rateCurrenciesIndexPaths, with: .none)
        }
    }
    
    private func refreshBaseCurrencyCell() {
        self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableViewRowAnimation.none)
    }
    
    private func didTextChangedOnBaseCurrency(numbericText: String) {
        guard let baseCurrencyAmount = Double(numbericText) else {
            return
        }
        timer?.invalidate()
        baseCurrency.amount = baseCurrencyAmount
        startUpdateMechanism()
    }
    
    private func setBaseCurrency(from indexPath: IndexPath) {
        timer?.invalidate()
        let rateCurrency = rateCurrencies[indexPath.row - 1]
        let newBaseCurrency = rateCurrency.convertToBase()
        let newRateCurrency = baseCurrency.convertToCurrencyRate()
        rateCurrencies.remove(at: indexPath.row)
        baseCurrency = newBaseCurrency
        rateCurrencies.insert(newRateCurrency, at: 0)
        let destinationIndexPath = IndexPath(row: 0, section: 0)
        tableView.moveRow(at: indexPath, to: destinationIndexPath)
        refreshBaseCurrencyCell()
        startUpdateMechanism()
    }
}

extension CurrencyViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  
        //If user has selected the the row that contains base currency just focus.
        if let baseCurrencyCell = tableView.cellForRow(at: indexPath) as? CurrencyTableViewCell, indexPath.row == 0 {
            baseCurrencyCell.focusOnCurrencyRateTextField()
            return
        } else {
            if let baseCurrencyCell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? CurrencyTableViewCell {
                baseCurrencyCell.unfocusOnCurrencyRateTextField()
            }
            setBaseCurrency(from: indexPath)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CurrencyTableViewCell.height
    }

}

extension CurrencyViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            return CurrencyTableViewCell.dequeue(tableView: tableView, delegate: self, currencyData: baseCurrency, indexPath: indexPath)
        } else {
            let currencyData = rateCurrencies[indexPath.row - 1]
            return CurrencyTableViewCell.dequeue(tableView: tableView, delegate: self, currencyData: currencyData, indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rateCurrencies.count + 1
    }
}

//We do that to make the scroll smoother.
extension CurrencyViewController: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let timer = self.timer, !timer.isValid {
            startUpdateMechanism()
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if let timer = self.timer, !timer.isValid {
            startUpdateMechanism()
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        timer?.invalidate()
    }

}

extension CurrencyViewController: CurrencyTableViewCellDelegate {
    func currencyTableViewCellTextFieldChanged(cell: CurrencyTableViewCell, text: String) {
        didTextChangedOnBaseCurrency(numbericText: text)
    }
}

