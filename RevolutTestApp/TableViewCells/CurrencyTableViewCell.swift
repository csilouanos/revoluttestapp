//
//  CurrencyTableViewCell.swift
//  RevolutTestApp
//
//  Created by Silouanos on 19/09/2018.
//  Copyright © 2018 Silouanos. All rights reserved.
//

import UIKit
import SDWebImage

protocol CurrencyTableViewCellDelegate: class {
    func currencyTableViewCellTextFieldChanged(cell: CurrencyTableViewCell, text: String)
}

class CurrencyTableViewCell: UITableViewCell {

    static let identifier = "CurrencyTableViewCell"
    static let height = CGFloat(75)
    
    @IBOutlet weak var currencyRateTextField: UITextField!
    @IBOutlet weak var currencyCodeLabel: UILabel!
    @IBOutlet weak var currencyImageView: UIImageView!
    //Used only in case image url is broken!
    @IBOutlet weak var currencyLabel: UILabel!
    weak var delegate: CurrencyTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        configureCurrencyRateTextField()
    }
    
    private func configureCurrencyRateTextField() {
        currencyRateTextField.addTarget(self, action: #selector(CurrencyTableViewCell.didTextChanged(textField:)), for: .editingChanged)
    }
    
    var currencyData: CurrencyPresenter! {
        didSet {
            currencyRateTextField.text = String(format: "%.2f", currencyData.finalAmount)
            currencyCodeLabel.text = currencyData.shortDescription
            self.currencyImageView.isHidden = true
            self.currencyLabel.isHidden = false
            self.currencyLabel.text = self.currencyData.iconEmoji
            
            //Don't enable text field in case is not the base currency.
            if let _ = currencyData as? BaseCurrency {
                currencyRateTextField.isEnabled = true
            } else {
                currencyRateTextField.isEnabled = false
            }
        }
    }
    
    @objc func didTextChanged(textField: UITextField) {
        delegate.currencyTableViewCellTextFieldChanged(cell: self, text: textField.text ?? "")
    }
    
    /**
        Focuses on cell's text field.
    */
    func focusOnCurrencyRateTextField() {
        currencyRateTextField.becomeFirstResponder()
    }
    
    func unfocusOnCurrencyRateTextField() {
        currencyRateTextField.resignFirstResponder()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        currencyImageView.layer.masksToBounds = true
        currencyImageView.layer.cornerRadius = currencyImageView.frame.size.width / 2.0
    }
    
    class func dequeue(tableView: UITableView, delegate: CurrencyTableViewCellDelegate, currencyData: CurrencyPresenter, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyTableViewCell.identifier, for: indexPath) as? CurrencyTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = delegate
        cell.currencyData = currencyData
        return cell
    }
}
