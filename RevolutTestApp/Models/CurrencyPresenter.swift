//
//  CurrencyPresenter.swift
//  RevolutTestApp
//
//  Created by Silouanos on 18/09/2018.
//  Copyright © 2018 Silouanos. All rights reserved.
//

import Foundation

protocol CurrencyPresenter {
    var icon: String { get }
    var iconEmoji: String { get }
    var shortDescription: String { get }
    var finalAmount: Double { get }
}

extension CurrencyRate: CurrencyPresenter {
    var icon: String {
        return countryImageURL
    }
    
    var iconEmoji: String {
        return countryEmoji
    }
    
    var shortDescription: String {
        return currency
    }
    
    var finalAmount: Double {
        get {
            return self.calculatedAmount
        }
        set {
            self.calculatedAmount = finalAmount
        }
    }
}

extension BaseCurrency: CurrencyPresenter {
    var icon: String {
        return countryImageURL
    }
    
    var iconEmoji: String {
        return countryEmoji
    }

    var shortDescription: String {
        return currency
    }
    
    var finalAmount: Double {
        get {
            return self.amount
        }
        set {
            self.amount = finalAmount
        }
    }
}
