//
//  RevolutTestAppTests.swift
//  RevolutTestAppTests
//
//  Created by Silouanos on 17/09/2018.
//  Copyright © 2018 Silouanos. All rights reserved.
//

import XCTest
@testable import RevolutTestApp

class RevolutTestAppTests: XCTestCase {
    
    private var sampleBaseCurrency = BaseCurrency(currency: "NZD", amount: 1.45)
    private var sampleCurrencyRates = [CurrencyRate]()
    
    //Contains the calculated rates based on sampleBaseCurrency.
    private var sampleCurrencyCalculatedRates = [CurrencyRate]()

    private var parsedJSONResponse = [String : Any]()
    private let sampleResponseJSONString = "{\"base\":\"NZD\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.91786,\"BGN\":1.1106,\"BRL\":2.721,\"CAD\":0.87096,\"CHF\":0.64027,\"CNY\":4.5115,\"CZK\":14.602,\"DKK\":4.2342,\"GBP\":0.51006,\"HKD\":5.1857,\"HRK\":4.2214,\"HUF\":185.39,\"IDR\":9837.1,\"ILS\":2.3682,\"INR\":47.538,\"ISK\":72.567,\"JPY\":73.565,\"KRW\":740.87,\"MXN\":12.7,\"MYR\":2.7324,\"NOK\":5.5512,\"PHP\":35.542,\"PLN\":2.4521,\"RON\":2.6339,\"RUB\":45.185,\"SEK\":6.0136,\"SGD\":0.90856,\"THB\":21.652,\"TRY\":4.3316,\"USD\":0.66062,\"ZAR\":10.121,\"EUR\":0.56784}}"
    
    override func setUp() {
        super.setUp()
        convertJSONStringToDictonary()
        convertSampleCurrencyRates()
    }
    
    func convertJSONStringToDictonary() {
        if let parsedJSONResponse = convertToDictionary(text: sampleResponseJSONString) {
            self.parsedJSONResponse = parsedJSONResponse
        } else {
            XCTFail("String is not a json.")
        }
    }
    
    func convertSampleCurrencyRates() {
        let rateCurrenciesWithBase = Currency.parse(data: self.parsedJSONResponse)
        let rateCurrencies = rateCurrenciesWithBase.rateCurrencies
        sampleCurrencyRates.append(contentsOf: rateCurrencies)
        
        //Manually conversion
        sampleCurrencyRates.forEach({
            sampleCurrencyCalculatedRates.append(CurrencyRate(currency: $0.currency, rate: $0.calculatedAmount * sampleBaseCurrency.amount))
        })
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testCurrencyCalculations() {
        let realCalculatedCurrencyRates = Currency.convertBasedOnBaseCurrency(baseCurrency: sampleBaseCurrency, rateCurrencies: sampleCurrencyRates)
        for (index, currencyRate) in realCalculatedCurrencyRates.enumerated() {
                XCTAssert(currencyRate.calculatedAmount == sampleCurrencyCalculatedRates[index].calculatedAmount, "convertBasedOnBaseCurrency produces wrong results.")
        }
    }
    
    private func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

    
}
